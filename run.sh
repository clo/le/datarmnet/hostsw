#!/bin/bash

load_rmnetcore() {
insmod drivers/net/rmnetcore/rmnetcore.ko

lsmod | grep rmnetcore
}

if [[ $1 == "rmnetcore" ]]
then
	load_rmnetcore
	if [[ $2 == "-v" ]]
	then
		load_rmnetcore
	fi
elif [[ $1 == "" ]] || [[ $1 == "all" ]]
then
	load_rmnetcore
else
	echo "Usage: $0 <rmnetcore / all>"
	echo "Usage: $0 will load all modules"
fi
