/* SPDX-License-Identifier: GPL-2.0-only */
/* Copyright (c) 2013-2018, 2021, The Linux Foundation. All rights reserved.
 */

#ifndef _RMNET_MAP_H_
#define _RMNET_MAP_H_

#define RMNET_FLAGS_INGRESS_MAP_CKSUMV5           (1U << 4)
#define RMNET_FLAGS_EGRESS_MAP_CKSUMV5            (1U << 5)

/* rmnet_map_header flags field:
 *  PAD_LEN:	  number of pad bytes following packet data
 *  CMD:	  1 = packet contains a MAP command; 0 = packet contains data
 *  NEXT_HEADER: 1 = packet contains V5 CSUM header 0 = no V5 CSUM header
 */
#define MAP_PAD_LEN_MASK		GENMASK(5, 0)
#define MAP_NEXT_HEADER_FLAG		BIT(6)
#define MAP_CMD_FLAG			BIT(7)

#define MAP_CSUM_UL_OFFSET_MASK		GENMASK(13, 0)
#define MAP_CSUM_UL_UDP_FLAG		BIT(14)
#define MAP_CSUM_UL_ENABLED_FLAG	BIT(15)

/* MAP CSUM headers */
struct rmnet_map_v5_csum_header {
	u8 header_info;
	u8 csum_info;
	__be16 reserved;
} __aligned(1);


struct rmnet_map_header {
	u8 flags;			/* MAP_CMD_FLAG, MAP_PAD_LEN_MASK */
	u8 mux_id;
	__be16 pkt_len;			/* Length of packet, including pad */
}  __aligned(1);

struct rmnet_map_dl_csum_trailer {
	u8 reserved1;
	u8 flags;			/* MAP_CSUM_DL_VALID_FLAG */
	__be16 csum_start_offset;
	__be16 csum_length;
	__be16 csum_value;
} __aligned(1);

/* rmnet_map_dl_csum_trailer flags field:
 *  VALID:	1 = checksum and length valid; 0 = ignore them
 */
#define MAP_CSUM_DL_VALID_FLAG		BIT(0)

struct rmnet_map_ul_csum_header {
	__be16 csum_start_offset;
	__be16 csum_info;		/* MAP_CSUM_UL_* */
} __aligned(1);

/* csum_info field:
 *  OFFSET:	where (offset in bytes) to insert computed checksum
 *  UDP:	1 = UDP checksum (zero checkum means no checksum)
 *  ENABLED:	1 = checksum computation requested
 */
#define MAP_CSUM_UL_OFFSET_MASK		GENMASK(13, 0)
#define MAP_CSUM_UL_UDP_FLAG		BIT(14)
#define MAP_CSUM_UL_ENABLED_FLAG	BIT(15)


/* v5 header_info field
 * NEXT_HEADER: represents whether there is any next header
 * HEADER_TYPE: represents the type of this header
 *
 * csum_info field
 * CSUM_VALID_OR_REQ:
 * 1 = for UL, checksum computation is requested.
 * 1 = for DL, validated the checksum and has found it valid
 */

#define MAPV5_HDRINFO_NXT_HDR_FLAG	BIT(0)
#define MAPV5_HDRINFO_HDR_TYPE_FMASK	GENMASK(7, 1)
#define MAPV5_CSUMINFO_VALID_FLAG	BIT(7)

#define RMNET_MAP_HEADER_TYPE_CSUM_OFFLOAD 2

struct rmnet_map_control_command {
	u8  command_name;
	u8  cmd_type:2;
	u8  reserved:6;
	u16 reserved2;
	u32 transaction_id;
	union {
		struct {
			u16 ip_family:2;
			u16 reserved:14;
			__be16 flow_control_seq_num;
			__be32 qos_id;
		} flow_control;
		u8 data[0];
	};
}  __aligned(1);

enum rmnet_map_commands {
	RMNET_MAP_COMMAND_NONE,
	RMNET_MAP_COMMAND_FLOW_DISABLE,
	RMNET_MAP_COMMAND_FLOW_ENABLE,
	/* These should always be the last 2 elements */
	RMNET_MAP_COMMAND_UNKNOWN,
	RMNET_MAP_COMMAND_ENUM_LENGTH
};

#define RMNET_MAP_COMMAND_REQUEST     0
#define RMNET_MAP_COMMAND_ACK         1
#define RMNET_MAP_COMMAND_UNSUPPORTED 2
#define RMNET_MAP_COMMAND_INVALID     3

#define RMNET_MAP_NO_PAD_BYTES        0
#define RMNET_MAP_ADD_PAD_BYTES       1

struct sk_buff *rmnet_map_deaggregate(struct sk_buff *skb,
				      struct rmnet_port *port);
struct rmnet_map_header *rmnet_map_add_map_header(struct sk_buff *skb,
						  int hdrlen,
						  struct rmnet_port *port,
						  int pad);
void rmnet_map_command(struct sk_buff *skb, struct rmnet_port *port);
int rmnet_map_checksum_downlink_packet(struct sk_buff *skb, u16 len);
void rmnet_map_checksum_uplink_packet(struct sk_buff *skb,
				      struct rmnet_port *port,
				      struct net_device *orig_dev,
				      int csum_type);
int rmnet_map_process_next_hdr_packet(struct sk_buff *skb, u16 len);

#endif /* _RMNET_MAP_H_ */
