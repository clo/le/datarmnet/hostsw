#!/bin/bash

unload_rmnetcore() {
rmmod rmnetcore

lsmod | grep rmnetcore
}

if [[ $1 == "rmnetcore" ]]
then
	unload_rmnetcore
elif [[ $1 == "" ]] || [[ $1 == "all" ]]
then
	unload_rmnetcore
else
	echo "Usage: $0 <rmnetcore / all>"
	echo "Usage: $0 will unload all modules"
fi
