#!/bin/bash
if [[ $1 == "rmnetcore" ]]
then
	make -C drivers/net/rmnetcore $2
elif [[ $1 == "clean" ]]
then
	make -C drivers/net/rmnetcore $1
elif [[ $1 == "" ]] || [[ $1 == "all" ]]
then
	make -C drivers/net/rmnetcore $2
else
	echo "Usage: $0 <rmnetcore / all / clean>"
	echo "$0 (with \"all\" or no arguments) will build all modules"
	echo "$0 clean or $0 all clean: will clean all modules"

	exit 127
fi

exit 0
